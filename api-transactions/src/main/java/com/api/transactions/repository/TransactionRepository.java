package com.api.transactions.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.api.transactions.entity.TransactionsEntity;

public interface TransactionRepository extends JpaRepository<TransactionsEntity, Long> {
	
	Optional<TransactionsEntity> findByReference(String reference);

	//List<TransactionsEntity> findByAccountIban(String accountIban, Pageable pageable);

}
