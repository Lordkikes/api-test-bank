package com.api.transactions.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.transactions.dao.AccountDao;
import com.api.transactions.dao.ResponseDao;
import com.api.transactions.dao.TransactionDao;
import com.api.transactions.entity.TransactionsEntity;
import com.api.transactions.repository.TransactionRepository;
import com.api.transactions.dao.util.ObjectMapperUtil;
import com.api.transactions.dao.util.StatusUtil;

@Service
public class TransactionServiceImpl implements TransactionService {

	private ObjectMapperUtil mapperUtil;
	private TransactionRepository repository;
	private AccountService accountService;

	@Autowired
	public TransactionServiceImpl(ObjectMapperUtil mapperUtil, TransactionRepository repository,
			AccountService accountService) {
		this.mapperUtil = mapperUtil;
		this.repository = repository;
		this.accountService = accountService;
	}

	@Override
	public List<TransactionDao> getAll(Pageable pageable) {
		return repository.findAll(pageable).stream().map(entity -> {
			return mapperUtil.entityToDto(TransactionDao.class, entity);
		}).collect(Collectors.toList());
	}

	@Override
	public TransactionDao create(TransactionDao dto) {
		createUpdateAccount(dto);

		if (repository.findByReference(dto.getReference()).isPresent()) {
			throw new IllegalArgumentException("La referencia ya existe");
		}
		TransactionsEntity entity = mapperUtil.dtoToEntity(TransactionsEntity.class, dto);
		return mapperUtil.entityToDto(TransactionDao.class, repository.save(entity));
	}

	@Override
	public ResponseDao getByReference(String reference, String channel) {
		return getResponseDao(reference, channel);
	}

	@Override
	public List<TransactionDao> getByAccountIban(String accountIban, String order) {
		// TODO Auto-generated method stub
		return null;
	}

	private void createUpdateAccount(TransactionDao dto) {

		Optional<AccountDao> accountDto = accountService.getByAccountIban(dto.getAccountIban());

		AccountDao accountSave = accountDto.map(account -> {
			Double amount = dto.getAmount();
			if (amount > 0) {
				amount += account.getAmount();
			} else if (account.getAmount() < Math.abs(dto.getAmount())) {
				// Math.abs : change a negative number to positive number
				throw new IllegalArgumentException("Saldo Insuficiente para un debito.");
			}
			amount = account.getAmount() - Math.abs(dto.getAmount());
			account.setAmount(amount);
			return account;
		}).orElseGet(() -> {
			Double amount = dto.getAmount();
			AccountDao account = new AccountDao();
			account.setAccountIban(dto.getAccountIban());
			if (dto.getAmount() < 0) {
				throw new IllegalArgumentException("Una cuenta nueva no puede inicar con un debito.");
			}
			account.setAmount(amount);
			return account;
		});
		accountService.create(accountSave);
	}
	
	private ResponseDao getResponseDao(String reference, String channel) {
		ResponseDao responseDao = new ResponseDao();
		Optional<TransactionsEntity> entity = repository.findByReference(reference);
		if (!entity.isPresent()) {
			responseDao.setReference(reference);
			responseDao.setStatus(StatusUtil.INVALID.getValue());
		} else {
			TransactionsEntity trnsEntity = entity.get();
			LocalDate transactionDate = trnsEntity.getDate().toLocalDate();
			String status = getStatus(transactionDate, channel);
			responseDao.setReference(reference);
			responseDao.setAmount(trnsEntity.getAmount());
			responseDao.setStatus(status);
			switch (channel) {
			case "CLIENT":
				if(StatusUtil.FUTURE.getValue().equals(status) || StatusUtil.PENDING.getValue().equals(status))
					responseDao.setAmount(trnsEntity.getAmount() - trnsEntity.getFee());
				break;
			case "ATM":
				if(StatusUtil.PENDING.getValue().equals(status))
					responseDao.setAmount(trnsEntity.getAmount() - trnsEntity.getFee());
				break;
			case "INTERNAL":
				responseDao.setFee(trnsEntity.getFee());
				break;
			default:
				throw new IllegalArgumentException("invalid channel");			
			}
		}
		return responseDao;

	}
	
	private String getStatus(LocalDate transactionDate, String channel) {
		String status = StatusUtil.INVALID.getValue();
		if (transactionDate.isBefore(LocalDate.now())) {
			status = StatusUtil.SETTLED.getValue();
		} else if (transactionDate.isEqual(LocalDate.now())) {
			status = StatusUtil.PENDING.getValue();
		} else if (transactionDate.isAfter(LocalDate.now())) {
			if("ATM".equals(channel)) {
				status = StatusUtil.PENDING.getValue();
			} else {
				status = StatusUtil.FUTURE.getValue();
			}
		}
		return status;
	}

}
