package com.api.transactions.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.api.transactions.dao.ResponseDao;
import com.api.transactions.dao.TransactionDao;

public interface TransactionService {
	
	public List<TransactionDao> getAll(Pageable pageable);

	public TransactionDao create(TransactionDao dto);

	public ResponseDao getByReference(String reference, String channel);

	public List<TransactionDao> getByAccountIban(String accountIban, String order);
}