package com.api.transactions.service;

import java.util.Optional;

import com.api.transactions.dao.AccountDao;

public interface AccountService {

	public AccountDao create(AccountDao dto);

	public Optional<AccountDao> getByAccountIban(String accountIban);
}
