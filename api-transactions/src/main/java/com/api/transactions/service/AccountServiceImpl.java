package com.api.transactions.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.transactions.dao.AccountDao;
import com.api.transactions.entity.AccountEntity;
import com.api.transactions.repository.AccountRepository;
import com.api.transactions.dao.util.ObjectMapperUtil;

@Service
public class AccountServiceImpl implements AccountService {
	
	private ObjectMapperUtil mapperUtil;
	private AccountRepository repository;
	
	@Autowired
	public AccountServiceImpl(ObjectMapperUtil mapperUtil, AccountRepository repository) {
		super();
		this.mapperUtil = mapperUtil;
		this.repository = repository;
	}

	@Override
	public AccountDao create(AccountDao dto) {
		AccountEntity entity = this.mapperUtil.dtoToEntity(AccountEntity.class, dto);
		return this.mapperUtil.entityToDto(AccountDao.class, this.repository.save(entity));
	}

	@Override
	public Optional<AccountDao> getByAccountIban(String accountIban) {
		return this.repository.findByAccountIban(accountIban).map( entity -> {
			return this.mapperUtil.entityToDto(AccountDao.class, entity);
		});
	}
}
