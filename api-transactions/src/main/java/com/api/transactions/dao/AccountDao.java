package com.api.transactions.dao;

import lombok.Data;

@Data
public class AccountDao {

	private Long Id;
	private String accountIban;
	private Double amount;
}
