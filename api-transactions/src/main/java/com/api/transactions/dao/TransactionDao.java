package com.api.transactions.dao;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class TransactionDao {
	private Long Id;
	private String reference;
	private String accountIban;
	private Double amount;
	private LocalDateTime date;
	private Double fee;
	private String description;
}
